/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "Region.h"
#include "Rect.h"
#include <string.h>

Region::Region(int NewType,const char *NewName)
{
  XType=NewType;
  User=0;

  if(!NewName) XName=0;
  else
  {
    XName=new char[strlen(NewName+1)];
    if(XName) strcpy(XName,NewName);
  }
}

Region::~Region()
{
  if(XName) delete [] XName;
  XName=0;
}

int RemoveOverlaps(Region **Buf,int Cur,int Max)
{
  Region *R1,*R2;
  int I,J,K,N,Added;
  
  for(J=K=0;J<Cur;J++)
  {
    for(I=J+1;I<Cur;I++)
    {
      R1=Buf[J];
      R2=Buf[I];

      if(R1->Intersects(*R2))
      {
        // No regions added yet
        Added=0;

        // Crop R2 with R1
        N=R1->Intersect(*R2,Buf+I,1);

        // If Intersect() was successful...
        // Add extruding parts of R1 to the Buf[]
        if(N>0) N=R1->Complement(*R2,Buf+Cur,Max<0? Max:Max>Cur? Max-Cur:0);

        // If Complement() was successful...
        // Add extruding parts of R2 to the Buf[]
        if(N>=0)
        {
          Added+=N;
          N=R2->Complement(*R1,Buf+Cur+Added,Max<0? Max:Max>Cur? Max-Cur:0);
        }

        // If Complement() was successful...
        // Wrap things up
        if(N>=0)
        {
          // New number of items in Buf[]
          Cur+=Added+N;

          // R1,R2 are no longer needed
          delete R1;
          delete R2;

          // As R1 is no longer present, we break out of the internal loop
          break;
        }

        // If we got here, one of operations was unsuccessful
        // and we need to clean up. In this case, some regions
        // remain overlapped.
        for(N=0;N<Added;N++) delete Buf[Cur+N];
      }
    }

    // If we broke out of the internal loop, Buf[J] was cut and
    // therefore it is no longer there. Otherwise, it has to be
    // copied.
    if(I==Cur) Buf[K++]=Buf[J];
  }

  // Return new number of regions
  return(K);
}

std::ostream &operator <<(std::ostream &Out,const Region &R)
{
  Point *PP;
  Rect *PR;


  if(R.Name())  { Out<<R.Name();return(Out); }
  if(R.Empty()) { Out<<"<EMPTY>";return(Out); }

  switch(R.Type())
  {
    case REG_POINT:
      PP=(Point *)&R;
      Out<<"("<<PP->X()<<","<<PP->Y()<<")";
      break;

    case REG_RECT:
      PR=(Rect *)&R;
      Out<<"("<<PR->X1()<<","<<PR->Y1()
         <<")-("<<PR->X2()<<","<<PR->Y2()<<")";
      break;

    default:
      Out<<"<REGION>";
      break;
  }

  return(Out);
}
