/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef RECT_H
#define RECT_H

#include "Region.h"
#include <math.h>

#define MAXFLOAT  ((double)3.40282346638528860e+38) 

class Rect;

class RectData
{
  public:
    static const RectData EmptyRectData;

    RectData() { PX1=PY1=MAXFLOAT;PX2=PY2=-MAXFLOAT; }

    RectData(double NX1,double NY1,double NX2,double NY2)
    {
      if(NX1<NX2) { PX1=NX1;PX2=NX2; } else { PX1=NX2;PX2=NX1; }
      if(NY1<NY2) { PY1=NY1;PY2=NY2; } else { PY1=NY2;PY2=NY1; }
    }

    double X1(void) const { return(PX1); }
    double Y1(void) const { return(PY1); }
    double X2(void) const { return(PX2); }
    double Y2(void) const { return(PY2); }

    bool operator !(void) const { return(PX1>PX2); }

    bool operator ==(const RectData &R) const
    { return((PX1==R.PX1)&&(PX2==R.PX2)&&(PY1==R.PY1)&&(PY2==R.PY2)); }

    bool operator !=(const RectData &R) const
    { return(!(*this==R)); }

    const RectData operator +(const RectData &R) const;
    const RectData operator *(const RectData &R) const;

    const RectData &operator +=(const RectData &R)
    { *this=*this+R;return(*this); }

  protected:
    double PX1,PY1,PX2,PY2;
};

class Rect: public Region, public RectData
{
  public:
    static const Rect &EmptyRect(void);

    Rect(const Rect &R)
      : Region(REG_RECT),
        RectData(R)
    {}

    Rect(const Region &R)
      : Region(REG_RECT)
    { *this=R; }

    Rect(double NX1,double NY1,double NX2,double NY2,const char *NewName=0)
      : Region(REG_RECT,NewName),
        RectData(NX1,NY1,NX2,NY2)
    {}

    Rect(const char *NewName=0)
      : Region(REG_RECT,NewName),
        RectData(-MAXFLOAT,-MAXFLOAT,MAXFLOAT,MAXFLOAT)
    {}

    Rect(const RectData &R,const char *NewName=0)
      : Region(REG_RECT,NewName),
        RectData(R)
    {}

    virtual int Empty(void) const { return(!*this); }
    virtual int Intersects(const Region &R) const;
    virtual int Contains(const Region &R) const;

    virtual double Area(void) const { return(Empty()? 0:(PX2-PX1)*(PY2-PY1)); }
    virtual double MinD(const Region &R) const;
    virtual double MaxD(const Region &R) const;

    virtual const char *Attr(const char *Name) const;

    virtual const Region &Box(void) const { return(*this); }
    virtual const Region &Intersect(const Region &R) const;

    virtual int Intersect(const Region &R,Region **Out,int Max=-1) const;
    virtual int Complement(const Region &R,Region **Out,int Max=-1) const;

    virtual Region *Duplicate(void) const { return(new ::Rect(*this)); }

    virtual const Region &operator =(const Region &R);
};

class Point: public Rect
{
  public:
    Point(double NX,double NY,const char *NewName=0): Rect(NX,NY,NX,NY,NewName)
    { XType=REG_POINT; }

    Point(const char *NewName=0): Rect(0,0,0,0,NewName)
    { XType=REG_POINT; }

    double X(void) const { return(PX1); }
    double Y(void) const { return(PY1); }

    virtual Region *Duplicate(void) const { return(new ::Point(*this)); }

    virtual const Region &operator =(const Region &R);
};

#endif /* RECT_H */
