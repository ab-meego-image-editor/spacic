/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "Rect.h"
#include "Attrs.h"
#include <iostream>
#include <math.h>
#include <cstring>

using std::cerr;
using std::endl;

const RectData RectData::EmptyRectData;

const Rect &Rect::EmptyRect(void)
{
  static Rect A;
  (RectData &)A = RectData::EmptyRectData;
  return(A);
}

double Rect::MinD(const Region &R) const
{
  Rect *P;
  double DX,DY;

  if((R.Type()!=REG_RECT)&&(R.Type()!=REG_POINT)) return(0.0);
  P=(Rect *)&R;

  // Rectangles coincide vertically
  if((PX1<=P->PX2)&&(PX2>=P->PX1))
    return(PY2<P->PY1? P->PY1-PY2:P->PY2<PY1? PY1-P->PY2:0.0);

  // Rectangles coincide horizontally
  if((PY1<=P->PY2)&&(PY2>=P->PY1))
    return(PX2<P->PX1? P->PX1-PX2:P->PX2<PX1? PX1-P->PX2:0.0);

  // No coincidence, use SQRT(DX^2+DY^2)
  DX=PX2<P->PX1? P->PX1-PX2:PX1>P->PX2? PX1-P->PX2:0.0;
  DY=PY2<P->PY1? P->PY1-PY2:PY1>P->PY2? PY1-P->PY2:0.0;
  return(sqrt(DX*DX+DY*DY));
}

double Rect::MaxD(const Region &R) const
{
  Rect *P;
  double DX,DY,D;

  if((R.Type()!=REG_RECT)&&(R.Type()!=REG_POINT)) return(0.0);
  P=(Rect *)&R;

  DX = P->PX2>PX1? P->PX2-PX1:PX1-P->PX2;
  D  = P->PX1>PX2? P->PX1-PX2:PX2-P->PX1;
  if(D>DX) DX=D;
  DY = P->PY2>PY1? P->PY2-PY1:PY1-P->PY2;
  D  = P->PY1>PY2? P->PY1-PY2:PY2-P->PY1;
  if(D>DY) DY=D;
  return(sqrt(DX*DX+DY*DY));
}

int Rect::Intersects(const Region &R) const
{
  Rect *P;

  // Empty rectangle intersects nothing
  if(R.Empty()||Empty()) return(0);

  // Check argument type
  if((R.Type()!=REG_RECT)&&(R.Type()!=REG_POINT)) return(0);
  P=(Rect *)&R;

  return((PX2>=P->PX1)&&(PX1<=P->PX2)&&(PY2>=P->PY1)&&(PY1<=P->PY2));
}

int Rect::Contains(const Region &R) const
{
  Rect *P;

  // Empty rectangle contained in everything and contains nothing
  if(R.Empty()) return(1);
  if(Empty())   return(0);

  // Check argument type
  if((R.Type()!=REG_RECT)&&(R.Type()!=REG_POINT)) return(0);
  P=(Rect *)&R;

  return((PX2>=P->PX2)&&(PX1<=P->PX1)&&(PY2>=P->PY2)&&(PY1<=P->PY1));
}

const char *Rect::Attr(const char *Name) const
{
  const char *P;

  // No attributes
  if(!User) return(0);

  // Look for name
  for(P=User;*P&&strcmp(P,Name);)
  {
    // Move to the argument value
    P+=strlen(P)+1;
    // Move to the next argument name
    P+=strlen(P)+1;
  }  

  // Return attribute value
  return(*P? (P+strlen(P)+1):0);
}

const Region &Rect::Intersect(const Region &R) const
{
  double TX1,TY1,TX2,TY2;
  Rect *P;
static Rect RBuf;
  // Empty rectangle intersects nothing
  if(R.Empty()||Empty()) return(EmptyRect());

  // Check argument type
  if((R.Type()!=REG_RECT)&&(R.Type()!=REG_POINT)) return(EmptyRect());
  P=(Rect *)&R;

  // Compute intersection
  TX1=PX1>P->PX1? PX1:P->PX1;
  TX2=PX2<P->PX2? PX2:P->PX2;
  TY1=PY1>P->PY1? PY1:P->PY1;
  TY2=PY2<P->PY2? PY2:P->PY2;

  // No intersection -> return NULL
  if((TX1>TX2)||(TY1>TY2)) return(EmptyRect());

  // Intersection result
  return(RBuf=Rect(TX1,TY1,TX2,TY2));
}

int Rect::Intersect(const Region &R,Region **Out,int Max) const
{
  double TX1,TY1,TX2,TY2;
  const Rect *P;

  // Empty rectangle intersects nothing
  if(R.Empty()||Empty()) return(0);

  // Check argument type
  if((R.Type()!=REG_RECT)&&(R.Type()!=REG_POINT)) return(-1);
  P=(Rect *)&R;

  // Compute intersection
  TX1=PX1>P->PX1? PX1:P->PX1;
  TX2=PX2<P->PX2? PX2:P->PX2;
  TY1=PY1>P->PY1? PY1:P->PY1;
  TY2=PY2<P->PY2? PY2:P->PY2;

  // No intersection -> return NULL
  if((TX1>TX2)||(TY1>TY2)) return(0);

  // Check if there is space in Out
  if(!Max) return(-1);

  // Add intersection result to Out
  *Out = new Rect(TX1,TY1,TX2,TY2);
  return(1);
}

int Rect::Complement(const Region &R,Region **Out,int Max) const
{
  double TX1,TX2;
  const Rect *P;
  int N;

  // Empty rectangle has no complement
  if(Empty()) return(0);

  // Check if there is space in Out
  if((Max>=0)&&(Max<4)) return(-1);

  // Check argument type
  if((R.Type()!=REG_RECT)&&(R.Type()!=REG_POINT)) return(-1);
  P=(Rect *)&R;
  N=0;

  // Complementing with empty rectangle gives input unchanged
  if(R.Empty()) { *Out=new Rect(*P);return(1); }

  // Left side
  if(P->PX1>PX1) Out[N++] = new Rect(PX1,PY1,P->PX1<PX2? P->PX1:PX2,PY2);

  // Right side
  if(P->PX2<PX2) Out[N++] = new Rect(P->PX2>PX1? P->PX2:PX1,PY1,PX2,PY2);

  if((P->PX1<PX2)&&(P->PX2>PX1)&&(P->PY1<PY2)&&(P->PY2>PY1))
  {
    TX1=P->PX1>PX1? P->PX1:PX1;
    TX2=P->PX2<PX2? P->PX2:PX2;

    // Upper side
    if(P->PY1>PY1) Out[N++] = new Rect(TX1,PY1,TX2,P->PY1);

    // Lower side
    if(P->PY2<PY2) Out[N++] = new Rect(TX1,P->PY2,TX2,PY2);
  }

  // Return number of rectangles added
  return(N);
}

const RectData RectData::operator +(const RectData &R) const
{
  if(!R)     return(*this);
  if(!*this) return(R);

  return
  (
    RectData
    (
      R.PX1<PX1? R.PX1:PX1,
      R.PY1<PY1? R.PY1:PY1,
      R.PX2>PX2? R.PX2:PX2,
      R.PY2>PY2? R.PY2:PY2
    )
  );
}

const RectData RectData::operator *(const RectData &R) const
{
  double TX1,TX2,TY1,TY2;

  if(!*this||!R) return(EmptyRectData);

  TX1=R.PX1>PX1? R.PX1:PX1;
  TX2=R.PX2<PX2? R.PX2:PX2;
  TY1=R.PY1>PY1? R.PY1:PY1;
  TY2=R.PY2<PY2? R.PY2:PY2;

  return((TX1>TX2)||(TY1>TY2)? EmptyRectData:RectData(TX1,TY1,TX2,TY2));
}

const Region &Rect::operator =(const Region &R)
{
  // Assign empty regions no matter what
  if(R.Empty()) { (RectData &)*this=RectData::EmptyRectData;return(*this); }

  switch(R.Type())
  {
    case REG_RECT:
      PX1=((Rect *)&R)->PX1;
      PY1=((Rect *)&R)->PY1;
      PX2=((Rect *)&R)->PX2;
      PY2=((Rect *)&R)->PY2;
      User=R.User;
      return(*this);

    case REG_POINT:
      PX1=PX2=((Point *)&R)->X();
      PY1=PY2=((Point *)&R)->Y();
      User=R.User;
      return(*this);
  }

  cerr<<"Error assigning regions: "<<*this<<" := "<<R<<endl;
  return(*this);
}

const Region &Point::operator =(const Region &R)
{
  // Assign empty regions no matter what
  if(R.Empty()) { (RectData &)*this=RectData::EmptyRectData;return(*this); }

  switch(R.Type())
  {
    case REG_RECT:
      if(((Rect *)&R)->X1()!=((Rect *)&R)->X2()) break;
      if(((Rect *)&R)->Y1()!=((Rect *)&R)->Y2()) break;
      PX1=PX2=((Rect *)&R)->X1();
      PY1=PY2=((Rect *)&R)->Y1();
      User=R.User;
      return(*this);

    case REG_POINT:
      PX1=PX2=((Point *)&R)->X1();
      PY1=PY2=((Point *)&R)->Y1();
      User=R.User;
      return(*this);
  }

  cerr<<"Error assigning regions: "<<*this<<" := "<<R<<endl;
  return(*this);
}
