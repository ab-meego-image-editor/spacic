##########

TEMPLATE = lib
TARGET = quillspacic
# Please do not remove this INCLUDEPATH in any case
INCLUDEPATH += .
DEPENDPATH += .

DEFINES     +=
QT -= core gui

CONFIG += release

# Generate pkg-config support by default
# Note that we HAVE TO also create prl config as QMake implementation
# mixes both of them together.
CONFIG += create_pc create_prl no_install_prl

#QMAKE_CXXFLAGS += -Werror
# --- input

INSTALL_HEADERS = Attrs.h \
	    PQTree.h \
	    PRTree.h \
	    Query.h \
	    Rect.h \
	    Region.h \
	    RTree.h \
	    WFF.h

HEADERS += $$INSTALL_HEADERS \
	    LimBoundary.h \
	    LimPriority.h \
	    LimTotal.h

SOURCES += Attrs.cpp        \
	    PQTree.cpp      \
	    PRTree.cpp      \
	    QIntersect.cpp  \
	    QJoin.cpp       \
	    QNeighbor.cpp   \
	    QRestrict.cpp   \
	    QSelect.cpp     \
	    QSubtract.cpp   \
	    Query.cpp       \
	    QUnion.cpp      \
	    Rect.cpp        \
	    Region.cpp      \
	    RTree.cpp       \
	    WFF.cpp

INSTALL_HEADERS = Attrs.h \
	    PQTree.h \
	    PRTree.h \
	    Query.h \
	    Rect.h \
	    Region.h \
	    RTree.h \
	    WFF.h

# --- install
headers.files = $$INSTALL_HEADERS
headers.path = $$[QT_INSTALL_HEADERS]/$$TARGET
target.path = $$[QT_INSTALL_LIBS]
pkgconfig.files = quillspacic.pc
pkgconfig.path = $$[QT_INSTALL_LIBS]/pkgconfig
prf.files = quillspacic.prf
prf.path = $$[QMAKE_MKSPECS]/features
INSTALLS += target headers pkgconfig prf

# ---clean
QMAKE_CLEAN += *.gcov *.gcno *.log *.moc_* *.gcda


