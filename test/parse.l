%{

#include "SWFF.h"
#include "y.tab.h"
#include <string.h>
#include <stdlib.h>

extern YYSTYPE yylval;
%}

ws	[ \t\n]

%%

dst		{ return(DIST); }
dist		{ return(DIST); }
\(		{ return(OP); }
\)		{ return(CP); }
\,		{ return(COMMA); }
\;		{ return(SEMICOL); }
[rR][0-9]*	{ yylval.string=strdup(yytext);yylval.string[0]='R';return(REGION); }
[dD][0-9]*	{ yylval.string=strdup(yytext);yylval.string[0]='D';return(DATASET); }
\<		{ yylval.integer=OP_LT;return(DOPCODE); }
\>		{ yylval.integer=OP_GT;return(DOPCODE); }
\<=		{ yylval.integer=OP_LE;return(DOPCODE); }
=\<		{ yylval.integer=OP_LE;return(DOPCODE); }
\>=		{ yylval.integer=OP_GE;return(DOPCODE); }
=\>		{ yylval.integer=OP_GE;return(DOPCODE); }
=		{ return(EQUAL); }
\<\>		{ yylval.integer=OP_NE;return(DOPCODE); }
!=		{ yylval.integer=OP_NE;return(DOPCODE); }
intersects	{ yylval.integer=OP_INTERSCT;return(SOPCODE); }
subseteq	{ yylval.integer=OP_SUBSETEQ;return(SOPCODE); }
subset		{ yylval.integer=OP_SUBSET;return(SOPCODE); }
equal		{ yylval.integer=OP_EQUAL;return(SOPCODE); }
nequal		{ yylval.integer=OP_NEQUAL;return(SOPCODE); }
prtree		{ yylval.integer=1;return(DATATYPE); }
pqtree		{ yylval.integer=2;return(DATATYPE); }
rtree		{ yylval.integer=3;return(DATATYPE); }
and		{ return(AND); }
or		{ return(OR); }
not		{ return(NOT); }
true		{ return(TRUE); }
T		{ return(TRUE); }
false		{ return(FALSE); }
F		{ return(FALSE); }
quit		{ return(QUIT); }
exit		{ return(QUIT); }
bye		{ return(QUIT); }
clear		{ return(CLEAR); }
time		{ return(TIME); }
load		{ return(LOAD); }
aselect		{ return(ASELECT); }
rselect		{ return(RSELECT); }
restrict	{ return(RESTRICT); }
neighbor	{ return(NEIGHBOR); }
split		{ return(SPLIT); }
merge		{ return(MERGE); }
\+		{ return(UNION); }
\*		{ return(INTERSECTION); }
\-		{ return(DIFFERENCE); }
{ws}		{}
<<EOF>>		{ yyterminate(); }

[+-]{0,1}[0-9]+((\.[0-9]*){0,1})	{
  yylval.value=atof(yytext);
  return(VALUE);
}

\"[^\n\t\"]+\"	{
  yylval.string=strdup(yytext+1);
  *(char *)strchr(yylval.string,'\"')='\0';
  return(STRING);
}

%%
