#!/usr/imports/bin/perl

$DATAPATH = "Data/Small";
$OUTPATH  = "Out";
$SEED     = 666;
$RUNS     = 4;
$POINTS   = 40;

print("Opening directory ".$DATAPATH."...");
opendir(DIR,$DATAPATH);
@Files = readdir(DIR);
closedir(DIR);
print(@Files." files...\n");

srand($SEED);
for($J=0;$J<$POINTS;$J++)
{
  do
  {
    $F1=int(rand(@Files-2)+2);
    $F2=int(rand(@Files-2)+2);

    for($K=0;$K<$J;$K++)
    {
      if($F1==$F2) { last; }
      if(($FF1[$K]==$F1)&&($FF2[$K]==$F2)) { last; }
      if(($FF1[$K]==$F2)&&($FF2[$K]==$F1)) { last; }
    }
  } while($K<$J);

  $FF1[$J]=$F1;
  $FF2[$J]=$F2;
}

for($I=0;$I<$RUNS;$I++)
{
  for($J=0;$J<$POINTS;$J++)
  {
    $F1=$FF1[$J];
    $F2=$FF2[$J];
    print("[".($I+1)."] Computing for ".$Files[$F1]."(".$F1.") and ".$Files[$F2]."(".$F2.")\n");
    system("gzcat ".$DATAPATH."/".$Files[$F1]." >DATA1");
    system("gzcat ".$DATAPATH."/".$Files[$F2]." >DATA2");
    system("spacic <SCRIPT.spc >".$OUTPATH."/Run".$I."-".$J.".out");
    unlink("DATA1");
    unlink("DATA2");
  }
}

print("Analyzing ".$RUNS."x".$POINTS." files...\n");
open(OUTFILE,">ANAL.out");

for($J=0;$J<$POINTS;$J++)
{
  for($I=0;$I<$RUNS;$I++)
  {
    open(INFILE,"<".$OUTPATH."/Run".$I."-".$J.".out");
    $K=0;

    while(<INFILE>)
    {
      if(/^Returned ([0-9]+) regions in ([0-9]+)ms/)
      {
        $Items[$K] = $I? $1:($Items[$K]+$1);
        $Times[$K] = $I? $2:($Times[$K]+$2);
        $K++;
      }
    }
  }

  close(INFILE);
  for($I=0;$I<$K;$I++) { $Items[$I]=$Items[$I]/3; }
  print OUTFILE $Items[0]+$Items[1];
  for($I=6;$I<$K;$I++) { print OUTFILE "\t".$Times[$I]; }
  print OUTFILE "\n";
}

close(OUTFILE);
print("DONE!\n");
