/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef DRTREE_H
#define DRTREE_H

#include "PageBuffer.h"
#include "Query.h"
#include "Rect.h"

class DRTree: public QData,PageBuffer
{
  public:
    enum { RegsPerNode=31 };
    // Number of regions to be stored in a single R-Tree node (page).
 
    DRTree(const char *FileName,const char *NewName=0);
    ~DRTree();

    virtual int Add(const Region &R);
    virtual int Delete(const Region &R);
    virtual int Cost(const Region &R) const;
    virtual int Items(const Region &R) const;
    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

    void TestTree(int Node=-1);

  private:
    class THead
    {
      public:
        THead();
        int Root;
        int ItemCount;
        int Depth;
        RectData Box;
    };
    class TNode
    {
      public:
        TNode();
        RectData Box;
        RectData Data[RegsPerNode];
        int Child[RegsPerNode];
        int Parent;
        int Leaf;
        int IItems;
        int EItems;
        int UseMask;
    };
    class QCType
    {
      public:
        Rect LastData;
        int NextNode;
        int NextDegree;
        int NextSlot;
    };

    int Root;
    int Depth;

    int InsertToNode(int Parent,const Rect &R,int Child=-1);
    int SplitAndInsert(int Parent,const Rect &R,int Child=-1);
    int UpdateCounts(int First,int EItems,int IItems,int Child,RectData R);
    int ChooseSubtree(const Rect &R);
    double ComputeOver(const Rect &R1,const Rect &R2) const;
    double ComputeArea(const Rect &R1,const Rect &R2) const;
};

#endif /* DRTREE_H */
