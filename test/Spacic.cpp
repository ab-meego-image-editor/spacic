/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "PRTree.h"
#include "PQTree.h"
#include "RTree.h"
#include "Query.h"

#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int ACount,QCount,RCount;

Query  *Queries[1000];
Region *Regions[1000];
WFF    *WFFs[1000];

int GetRegion(const char *Name,int Create=0);
int GetDataset(const char *Name,int Create=0);
void Execute(Query *Input,QData *Output=0,int Echo=1);
void Flush(int Everything=0);
int yyparse(void);

void Flush(int Everything)
{
  int I,J;

  // WFFs are always expendable
  for(J=0;J<ACount;J++) delete WFFs[J];
  ACount=0;

  if(Everything)
  {
    // All queries and datasets go
    for(J=0;J<QCount;J++) delete Queries[J];
    QCount=0;
    // Leave the first region (i.e. R)
    for(J=1;J<RCount;J++) delete Regions[J];
    RCount=1;
  }
  else
  {
    // Remove all unnamed queries
    for(J=0,I=0;J<QCount;J++)
      if((Queries[J]->Type()==QU_DATA)&&(((QData *)Queries[J])->Name()))
        Queries[I++]=Queries[J];
      else
        delete Queries[J];

    // New set of queries only includes named datasets
    QCount=I;

    // Remove all unnamed regions
    for(J=0,I=0;J<RCount;J++)
      if(Regions[J]->Name()) Regions[I++]=Regions[J];
      else delete Regions[J];
    
    // New set of regions only includes named ones
    RCount=I;
  }
}

void Execute(Query *Input,QData *Output,int Echo)
{
  QContext QC;
  clock_t TM1,TM2;
  Region *R;
  int J,I;  
 
  cout<<"Executing query '"<<*Input<<"' of cost "<<Input->Cost()<<"..."<<endl;
  
  // Save starting clock
  TM1=clock();

  // Execute query
  if(Echo) J=Output? Output->Load(*Input):Input->Print(cout);
  else
    for(J=0,R=Input->GetFirst(QC,&I);R;R=Input->GetNext(QC,0,&I))
      if(!I) J++;    

  // Save finishing clock
  TM2=clock();

  // Done
  cout<<"Returned "<<J<<" regions in "<<(double)(TM2-TM1)/1000.0<<"ms."<<endl;
}

int GetRegion(const char *Name,int Create)
{
  int J;

  // Find region by name
  for(J=0;J<RCount;J++)
    if(Regions[J]->Name()&&!strcmp(Regions[J]->Name(),Name))
      return(J);

  // Are we allowed to create a new region?
  if(!Create) return(-1);

  // If not found, create new region
  Regions[RCount] = new Rect(Name);
  return(RCount++);
}

int GetDataset(const char *Name,int Create)
{
  int J;

  // Find dataset by name
  for(J=0;J<QCount;J++)
    if(Queries[J]->Type()==QU_DATA)
      if(((QData*)Queries[J])->Name())
        if(!strcmp(((QData *)Queries[J])->Name(),Name))
          return(J);

  // If not found, create new dataset
  switch(Create)
  {
    case 1: Queries[QCount] = new PRTree(Name);return(QCount++);
    case 2: Queries[QCount] = new PQTree(Name);return(QCount++);
    case 3: Queries[QCount] = new RTree(Name);return(QCount++);
  }

  // Dataset not created
  return(-1);
}

void yyerror(const char *S)
{
  cerr<<S<<endl;
}

int main(int argc,char *argv[])
{
  int J;

  cout<<"Spatial BASIC v.0.1 by Marat Fayzullin (C)2000"<<endl<<endl;

  // Nothing yet
  QCount=ACount=RCount=0;

  // Create region R used for selects
  GetRegion("R",1);

  // Call parser!
  yyparse();

  // Destroy everything
  for(J=0;J<QCount;J++) delete Queries[J];
  for(J=0;J<ACount;J++) delete WFFs[J];
  for(J=0;J<RCount;J++) delete Regions[J];

  // Done
  return(0);
}
