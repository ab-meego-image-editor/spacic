%{

#include <stdio.h>
#include <string.h>
#include "Region.h"
#include "Rect.h"
#include "Query.h"
#include "GNIS.h"
#include "SWFF.h"

extern int ACount,QCount,RCount;

extern Query  *Queries[1000];
extern Region *Regions[1000];
extern SWFF   *SWFFs[1000];

int GetRegion(const char *Name,int Create=0);
int GetDataset(const char *Name,int Create=0);
void Execute(Query *Input,QData *Output=0,int Echo=1);
void Flush(int Everything=0);

%}

%union {
  char *string;
  double value;
  int integer;
}

%token <string> STRING
%token <string> REGION
%token <string> DATASET
%token <value> VALUE
%token <integer> SOPCODE
%token <integer> DOPCODE
%token <integer> DATATYPE
%token DIST SEMICOL OP COMMA CP AND OR NOT TRUE FALSE EOL EQUAL TIME
%token LOAD QUIT CLEAR ASELECT RSELECT RESTRICT NEIGHBOR SPLIT MERGE
%token UNION INTERSECTION DIFFERENCE

%type <integer> atom
%type <integer> satom
%type <integer> datom
%type <integer> reg
%type <integer> point
%type <integer> rect
%type <integer> swff
%type <integer> and
%type <integer> or
%type <integer> not
%type <integer> query
%type <integer> sopcode
%type <integer> dopcode

%left AND
%left OR
%left UNION
%left INTERSECTION
%left DIFFERENCE

%start start

%%

start:	cmd SEMICOL start
  |	cmd SEMICOL
  ;

cmd:	query			{
  if($1<0) cout<<"Error!"<<endl;
  else Execute(Queries[$1],0,1);
  Flush(0);
  }
  |	TIME query		{
  if($2<0) cout<<"Error!"<<endl;
  else Execute(Queries[$2],0,0);
  Flush(0);
  }
  |	reg			{
  if($1<0) cout<<"Error!"<<endl;
  else cout<<*Regions[$1]<<endl;
  }
  |	DATASET EQUAL query	{
  if($3<0) cout<<"Error!"<<endl;
  else Execute(Queries[$3],(QData *)Queries[GetDataset($1,1)],1);
  free($1);
  Flush(0);
  }
  |	DATATYPE DATASET EQUAL query	{
  if(($4<0)||(GetDataset($2,0)>=0)) cout<<"Error!"<<endl;
  else Execute(Queries[$4],(QData *)Queries[GetDataset($2,$1)],1);
  free($2);
  Flush(0);
  }
  |	REGION EQUAL reg		{
  if($3<0) cout<<"Error!"<<endl;
  else *Regions[GetRegion($1,1)]=*Regions[$3];
  free($1);
  Flush(0);
  }
  |	DATASET EQUAL reg		{
  if($3<0) cout<<"Error!"<<endl;
  else ((QData *)Queries[GetDataset($1,1)])->Add(*Regions[$3]);
  free($1);
  Flush(0);
  }
  |	DATATYPE DATASET EQUAL reg	{
  if($4<0) cout<<"Error!"<<endl;
  else ((QData *)Queries[GetDataset($2,$1)])->Add(*Regions[$4]);
  free($2);
  Flush(0);
  }
  |	CLEAR			{
  Flush(1);
  }
  |	QUIT			{
  return(0);
  }
  ;

query:	RSELECT OP query COMMA swff CP	{
  if(($3<0)||($5<0)) $$=-1;
  else
  {
    Queries[QCount] =
      new QRSelect(Queries[$3],SWFFs[$5],Regions[GetRegion("R",0)]);
    $$=QCount++;
  }
  }
  |	NEIGHBOR OP query COMMA reg CP	{
  if(($3<0)||($5<0)) $$=-1;
  else
  {
    Queries[QCount] = new QNeighbor(Queries[$3],Regions[$5]);
    $$=QCount++;
  }
  }
  |	RESTRICT OP query COMMA swff CP	{
  }
  |	SPLIT OP query CP		{
  }
  |	MERGE OP query CP		{
  }
  |	LOAD OP STRING CP		{
  Queries[QCount] = new GNIS($3);
  free($3);
  $$=QCount++;
  }
  |	query UNION query		{
  if(($1<0)||($3<0)) $$=-1;
  else
  {
    Queries[QCount] = new QUnion(Queries[$1],Queries[$3]);
    $$=QCount++;
  }
  }
  |	query INTERSECTION query	{
  if(($1<0)||($3<0)) $$=-1;
  else
  {
    Queries[QCount] = new QIntersect(Queries[$1],Queries[$3]);
    $$=QCount++;
  }
  }
  |	query DIFFERENCE query		{
  if(($1<0)||($3<0)) $$=-1;
  else
  {
    Queries[QCount] = new QSubtract(Queries[$1],Queries[$3]);
    $$=QCount++;
  }
  }
  |	DATASET				{
  $$=GetDataset($1,0);
  free($1);
  }
  |	OP query CP			{
  $$=$2;
  }
  ;

swff:	or
  ;

or:	or OR or	{
  SWFFs[ACount] = new SWFF(OP_OR,SWFFs[$1],SWFFs[$3]);
  $$=ACount++;
  }
  |	and
  ;

and:	and AND and	{
  SWFFs[ACount] = new SWFF(OP_AND,SWFFs[$1],SWFFs[$3]);
  $$=ACount++;
  }
  |	not
  ;

not:	NOT not		{
  SWFFs[ACount] = new SWFF(OP_NOT,SWFFs[$2],0);
  $$=ACount++;
  }
  |	atom
  ;
	
atom:	TRUE		{
  SWFFs[ACount] = new SWFF(OP_TRUE,(SWFF *)0,(SWFF *)0);
  $$=ACount++;
  }
  |	FALSE		{
  SWFFs[ACount] = new SWFF(OP_FALSE,(SWFF *)0,(SWFF *)0);
  $$=ACount++;
  }
  |	OP atom CP	{
  $$=$2;
  }
  |	satom
  |	datom
  ;

satom:	reg sopcode reg	{
  SWFFs[ACount] = new SWFF($2,Regions[$1],Regions[$3]);
  $$=ACount++;
  }
  ;

datom:	DIST OP reg COMMA reg CP dopcode VALUE	{
  SWFFs[ACount] = new SWFF($7,Regions[$3],Regions[$5],$8);
  $$=ACount++;
  }
  |	VALUE dopcode DIST OP reg COMMA reg CP	{
  switch($2)
  {
    case OP_LE: $2=OP_GE;break;
    case OP_LT: $2=OP_GT;break;
    case OP_GE: $2=OP_LE;break;
    case OP_GT: $2=OP_LT;break;
  } 
  SWFFs[ACount] = new SWFF($2,Regions[$5],Regions[$7],$1);
  $$=ACount++;
  }
  |	DIST OP reg COMMA reg CP dopcode DIST OP reg COMMA reg CP	{
  SWFFs[ACount] = new SWFF($7,Regions[$3],Regions[$5],Regions[$10],Regions[$12]);
  $$=ACount++;
  }
  ;

sopcode: SOPCODE
  |	 EQUAL		{ $$=OP_EQUAL; }
  ;

dopcode: DOPCODE
  |	 EQUAL		{ $$=OP_EQ; }
  ;

reg:	REGION	{
	$$=GetRegion($1,0);
	free($1);
	}
  |	point
  |	rect
  ;

point:	OP VALUE COMMA VALUE CP	{
	Regions[RCount] = new Point($2,$4);
	$$=RCount++;
	}
  ;

rect:	OP VALUE COMMA VALUE CP DIFFERENCE OP VALUE COMMA VALUE CP	{
	Regions[RCount] = new Rect($2,$4,$8,$10);
	$$=RCount++;
	}
  ;

%%
